package fibonacci

import groovy.transform.CompileStatic
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.FlowableEmitter
import io.reactivex.FlowableOnSubscribe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.SingleEmitter

import javax.inject.Inject

@CompileStatic
@Controller('/')
class FiboController {

    FiboService fiboService

    FiboController(FiboService fiboService){
        this.fiboService = fiboService
    }


    @Get('{fb}')
    Flowable<Number> index(long fb){
        Flowable.create({ FlowableEmitter emitter->
            for(BigInteger iter=0; iter < fb; iter++){

                emitter.onNext(fiboService.fib( iter ))

            }
            emitter.onComplete()
        }, BackpressureStrategy.ERROR)
    }

    @Get('/single/{fb}')
    Single<Number> single(long fb){
        Single.create({ SingleEmitter emitter->
            BigInteger result
            for(BigInteger iter=0; iter < fb; iter++){
                result = fiboService.fib( iter )
            }
            emitter.onSuccess( result )
        })
    }


}
