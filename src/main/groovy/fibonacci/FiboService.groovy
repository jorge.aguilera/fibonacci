package fibonacci

import groovy.transform.Memoized

import javax.inject.Singleton

@Singleton
class FiboService {

    @Memoized
    BigInteger fib(BigInteger n) {
        n<2 ? 1 : fib(n-1)+fib(n-2)
    }


}
