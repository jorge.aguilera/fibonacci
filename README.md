# Fibonacci Micronaut

Un simple endpoint hecho en Micronaut que sirve los primeros N números de una serie fibonacci

## Requisisitos

Java 8

## Build

`./gradlew build`

## Run

`./gradlew run`

## Test

curl localhost:8080/10 

[1,1,2,3,5,8,13,21,34,55]


curl localhost:8080/single/10 

55

